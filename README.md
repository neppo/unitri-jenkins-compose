# jenkins-compose


Up a instance of jenkins in your machine in port 8080.

### Getting started

1. Download the repository:

```sh
  git clone https://bitbucket.org/neppo/unitri-jenkins-compose.git
```

2. Open the folder:
```sh
  cd jenkins-compose
```


3. Start the container:
```sh
  docker-compose up
```

4. Open in a browser : [localhost:8080](http:localhost:8080)
